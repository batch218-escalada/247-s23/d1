// console.log('Hello World');

// [SECTION] Objects

    // An object is a data type that is used to represent real world objects.

    // [SUB-SECTION] Creating Objects using object initializers / Literal Notation.

        let cellphone = {
            name: 'Nokia 3210',
            manufactureDate: 1999,
            color: 'Gray'
        };

        console.log('Result from creating objects using initializers / literal notation:');
        console.log(cellphone);

        /* 
            Syntax:
                let objectName = {
                    keyA: valueA,
                    keyB: valueB,
                };
        */

    // [SUB-SECTION] Creating Objects Using a Constructor Function

        // Creates a reusable function to create several object that have the same data structure.

        function Laptop(name, manufactureDate, color) {
            this.name = name;
            this.manufactureDate = manufactureDate;
            this.color = color;
        };

        // This is an object
            // The 'this' keyword allows us to assign a new object's properties by associating them with values received from a constructor function parameters.

        let laptop = new Laptop('Lenovo', 2008, 'Red');

        console.log('Result from creating objects using object constructors:');
        console.log(laptop);

        /* 
            Syntax:
                function ObjectName(keyA, keyB) {
                    this.keyA = keyA;
                    this.keyB = keyB;                   
                };
        */

        // class Car {
        //     constructor (name, manufacturer, color) {
        //         this.name = name;
        //         this.manufacturer = manufacturer;
        //         this.color = color;
        //     }
        // };

        // The 'new' operator creates an instance of an object.

        // let carA = new Car('Civic', 'Honda', 'White');
        // let carB = new Car('Corolla', 'Toyota', 'Red');

        // console.log(carA);
        // console.log(carB);

        let myLaptop = new Laptop('MacBook Air', 2020, 'Red');

        console.log('Result from creating objects using object constructors');
        console.log(myLaptop);

    // [SUB-SECTION] Creating Empty Objects

        let computer = [];
        let myComputer = new Object();

            console.log(computer);
            console.log(myComputer);

// [SECTION] Accessing Object Properties

    // Using the dot notation

    console.log('Result from dot notation: ' + myLaptop.name);

    // using the square bracket notation

    console.log('Result from the square bracket notation: ' + myLaptop['name']);

    // [SUB-SECTION] Accessing Array Objects

        let array = [laptop, myLaptop];

        console.log(array[0].name);
        // Differentiation between accessing array and object properties
        // This tells us that array[0] is an object using the dot notation.
        console.log(array[0]['name']);

// [SECTION] Initializing / Adding / Deleting / Reassigning Object Properties

    // Like any other variable in JS, objects may have their properties initialized/add after thje object was created/declared.

        let car = {};

    // [SUB-SECTION] Initializing / Adding Object Properties Using Dot Notation

        car.name = 'Honda Civic';
        console.log('Result from adding properties using dot notation:');
        console.log(car);

    // [SUB-SECTION] Initializing / Adding Object Properties Using Bracket Notation
    
        car['manufacture date'] = 2019;

        console.log(car['manufacture date']);
        console.log(car['Manufacture date']); /* Result ---> undefined */
        console.log(car);

    // [SUB-SECTION] Deleting Object Properties

        delete car['manufacture date'];
        console.log('Result from deleting properties: ');
        console.log(car);

    // [SUB-SECTION] Reassigning Object Properties

        car.name = 'Dodge Charger R/T';
        console.log('Result from reassigning properties');
        console.log(car);

// [SECTION] Object Methods

    // A method is a function which is a property of an object.

        let person = {
            name: 'John',
            talk: function () {
                console.log('Hello my name is ' + this.name);
            }
        };

        console.log(person);
        console.log('Result from object methods:');
        person.talk();

    // [SUB-SECTION] Adding Methods to Objects

        person.walk = function () {
            console.log(this.name + ' walked 25 steps forward.');
        };

        person.walk();

    // Methods are useful for creating reusable function that perform tasks related to objects.
    
    let friend = {
        firstName: 'Joe',
        lastName: 'Smith',
        address: {
            city: 'Austin',
            country: 'Texas',
        },
        emails: ['joe@mail.com', 'joesmith@email.xyz'],
        contact: ['09123456789', '0987654321'],
        introduce: function () {
            console.log('Hello my name is ' + this.firstName + ' ' + this.lastName + ' & I am from ' + this.address.city + ', ' + this.address.country);
            
        }
    };

    console.log(friend);
    friend.introduce();

    /* 
        MINI-ACTIVITY

        Hello my name is Joe Smith & I'm from Austin, Texas!
    */

// [SECTION] Real World Application of Objects

    /* 
        Scenario

        1. We would like to create a game that would have several pokemon interact with each other.
        2. Every pokemon would have the same set of stats, properties and functions.
    */

        let myPokemon = {
            name: 'Pikachu',
            level: 3,
            health: 100,
            attack: 50,
            tackle: function () {
                console.log('This pokemon tackled targetPokemon');
                console.log('targetPokemon\'s health is now reduce to _targetPokemonhealth_');
            },
            faint: function () {
                console.log('Pokemon fainted.');
            }
        };

        console.log(myPokemon);

        function Pokemon(name, level) {
            
            // Properties
            this.name = name;
            this.level = level;
            this.health = 2 * level;
            this.attack = level;

            // Methods
            this.tackle = function (target) {
                console.log(this.name + ' tackled' + target.name);
                console.log('targetPokemon\'s health is now reduced to ' + Number(target.health - this.attack));
            };

            this.faint = function () {
                console.log(this.name + ' fainted.');
            }
        };

        // Create new instances of the 'Pokemon' object each with their unique properties.

        let Pikachu = new Pokemon('Pikachu', 16);
        let Rattata = new Pokemon('Rattata', 8);

        Pikachu.tackle(Rattata);
        Rattata.faint();


